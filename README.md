# SV Notebooks

Notebooks for software verification.

## Setup

We use Python 3.8 (any other recent Python version should be fine as well).
Please install this according to your OS.

On Linux, it may be useful to use a *virtual environment*
in order to be able to install Python packages as user
without affecting other Python software:

```bash
# One-time setup:
python3 -m venv ~/venv-sv-notebooks  # or any other directory
# Before each use:
. ~/venv-sv-notebooks/bin/activate
```

Then install requirements with:

```bash
pip install -r requirements.txt
```

Install an SMT solver for PySMT with:

```bash
pysmt-install --z3
```

## Start

Start a Jupyter notebook server with:

```bash
jupyter notebook
```
